<?php

namespace Eolas\PHPSimpleETL\Tests\Transformer;

use Eolas\PHPSimpleETL\Transformer\SliceTransformer;

class SliceTransformerTest extends \PHPUnit_Framework_TestCase
{
    public function testNoSliceNullArgument()
    {
        $transformer = new SliceTransformer(null);

        $value = [1, 2, 3, 4, 5];

        $finalValue = $transformer->transform($value);
        $this->assertCount(5, $finalValue);
    }

    public function testNoSliceFalseArgument()
    {
        $transformer = new SliceTransformer(false);

        $value = [1, 2, 3, 4, 5];

        $finalValue = $transformer->transform($value);
        $this->assertCount(5, $finalValue);
    }

    public function testSizeSameAsSlice()
    {
        $transformer = new SliceTransformer(5);

        $value = [1, 2, 3, 4, 5];

        $finalValue = $transformer->transform($value);
        $this->assertCount(5, $finalValue);
    }

    public function testSlice()
    {
        $transformer = new SliceTransformer(3);

        $value = [1, 2, 3, 4, 5];

        $finalValue = $transformer->transform($value);
        $this->assertCount(3, $finalValue);
    }

    public function testSliceBiggerThanData()
    {
        $transformer = new SliceTransformer(3);

        $value = [1, 2];

        $finalValue = $transformer->transform($value);
        $this->assertCount(2, $finalValue);
    }
}
