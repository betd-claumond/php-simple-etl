<?php

namespace Eolas\PHPSimpleETL\Tests\Transformer;

use Eolas\PHPSimpleETL\Transformer\NonEmptySliceTransformer;

class NonEmptySliceTransformerTest extends \PHPUnit_Framework_TestCase
{
    public function testEmptyValues()
    {
        $transformer = new NonEmptySliceTransformer();

        $value = ['', '   ', '', null];

        $finalValue = $transformer->transform($value);
        $this->assertCount(0, $finalValue);
    }

    public function testFullOfValues()
    {
        $transformer = new NonEmptySliceTransformer();

        $value = [1, 2, 3, 4, 5];

        $finalValue = $transformer->transform($value);
        $this->assertCount(5, $finalValue);
    }

    public function testEndsWithEmptyValues()
    {
        $transformer = new NonEmptySliceTransformer();

        $value = [1, 2, 3, 4, 5, '', '   ', '', null];

        $finalValue = $transformer->transform($value);
        $this->assertCount(5, $finalValue);
    }

    public function testContainsSomeEmptyValues()
    {
        $transformer = new NonEmptySliceTransformer();

        $value = [1, 2, 3, 4, 5, '', '   ', '', null, 7, '', '   ', '', null];

        $finalValue = $transformer->transform($value);
        $this->assertCount(10, $finalValue);
    }
}
