<?php

namespace Eolas\PHPSimpleETL\Tests\Transformer;

use Eolas\PHPSimpleETL\Transformer\ArrayColumnRemoveTransformer;

class ArrayColumnRemoveTransformerTest extends \PHPUnit_Framework_TestCase
{
    public function testRemoveAllColumnsWithIntegerKeys()
    {
        $data = array(0 => '1', 1 => '2', 2 => '3');

        $columnIndexes = array(0, 1, 2);

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(0, $transformedData);
    }

    public function testRemoveColumnWithIntegerKeys()
    {
        $data = array(0 => '1', 1 => '2', 2 => '3');

        $columnIndexes = array(1);

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(2, $transformedData);

        $this->assertArrayHasKey(0, $transformedData);
        $this->assertArrayNotHasKey(1, $transformedData);
        $this->assertArrayHasKey(2, $transformedData);
    }

    public function testRemoveNoColumnWithIntegerKeys()
    {
        $data = array(0 => '1', 1 => '2', 2 => '3');

        $columnIndexes = array(3, 4, 5);

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(3, $transformedData);
    }

    public function testRemoveAllColumnsWithStringKeys()
    {
        $data = array('col1' => '1', 'col2' => '2', 'col3' => '3');

        $columnIndexes = array('col1', 'col2', 'col3');

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(0, $transformedData);
    }

    public function testRemoveColumnWithStringKeys()
    {
        $data = array('col1' => '1', 'col2' => '2', 'col3' => '3');

        $columnIndexes = array('col2');

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(2, $transformedData);

        $this->assertArrayHasKey('col1', $transformedData);
        $this->assertArrayNotHasKey('col2', $transformedData);
        $this->assertArrayHasKey('col3', $transformedData);
    }

    public function testRemoveNoColumnWithStringKeys()
    {
        $data = array('col1' => '1', 'col2' => '2', 'col3' => '3');

        $columnIndexes = array('col4', 'col5', 'col6');

        $transformer = new ArrayColumnRemoveTransformer($columnIndexes);

        $transformedData = $transformer->transform($data);

        $this->assertCount(3, $transformedData);
    }
}
