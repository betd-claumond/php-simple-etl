<?php

namespace Eolas\PHPSimpleETL\Tests\Reader;

use Eolas\PHPSimpleETL\Reader\CSVFileReader;
use Eolas\PHPSimpleETL\Transformer\ContentTransformer;
use Eolas\PHPSimpleETL\Transformer\NonEmptySliceTransformer;
use Eolas\PHPSimpleETL\Transformer\SliceTransformer;

class CSVFileReaderTest extends \PHPUnit_Framework_TestCase
{
    public function testSimpleRead()
    {
        // csv file with 3 column and 5 lines with default ; separator
        $filepath = '.\tests\resources\simplecsv.csv';

        $csvFileReader = $this->getInitializedCSVReader($filepath);

        $csvFileReader->readHeader();

        $csvFileReader->read();

        $this->assertColumnAndLinesNumbers(3, 4, $csvFileReader);

        $csvFileReader->end();
    }

    public function testSimpleReadPipeSeparator()
    {
        // csv file with 3 column and 5 lines with pipe separator
        $filepath = '.\tests\resources\simplecsv_pipeseparator.csv';

        $csvFileReader = $this->getInitializedCSVReader($filepath);

        $csvFileReader->setSeparator('|');

        $csvFileReader->readHeader();

        $csvFileReader->read();

        $this->assertColumnAndLinesNumbers(3, 4, $csvFileReader);

        $csvFileReader->end();
    }

    public function testNoHeaderFile()
    {
        // csv file with 3 column and 5 lines with default ; separator used as file without header
        $filepath = '.\tests\resources\simplecsv.csv';

        $csvFileReader = $this->getInitializedCSVReader($filepath);

        $csvFileReader->setLineForHeader(null);

        $csvFileReader->readHeader();

        $csvFileReader->read();

        $this->assertColumnAndLinesNumbers(3, 5, $csvFileReader, false);

        $csvFileReader->end();
    }

    public function testExtraEmptyColumn()
    {
        // csv file with 3 column and 5 lines of data with default ; separator and extra ; on each line
        $filepath = '.\tests\resources\simplecsv_extraemptycolumns.csv';

        $csvFileReader = $this->getInitializedCSVReader($filepath);

        $csvFileReader->readHeader();

        $csvFileReader->read();

        $this->assertColumnAndLinesNumbers(7, 4, $csvFileReader);

        $csvFileReader->end();
    }

    public function testExtraEmptyColumnWithSliceTransformer()
    {
        // csv file with 3 column and 5 lines of data with default ; separator and extra ; on each line
        $filepath = '.\tests\resources\simplecsv_extraemptycolumns.csv';

        $csvFileReader = $this->getInitializedCSVReader($filepath);

        $csvFileReader->addHeaderTransformer(new NonEmptySliceTransformer());

        $csvFileReader->readHeader();

        $columnCount = $csvFileReader->getColumnCount();
        $this->assertEquals(3, $columnCount);

        $csvFileReader->addDataTransformer(new ContentTransformer(new SliceTransformer($columnCount)));

        $csvFileReader->read();

        $this->assertColumnAndLinesNumbers(3, 4, $csvFileReader);

        $csvFileReader->end();
    }

    protected function getInitializedCSVReader($filepath)
    {
        $this->assertFileExists($filepath);

        $csvFileReader = new CSVFileReader($filepath);

        $initialized = $csvFileReader->init();
        $this->assertTrue($initialized);

        return $csvFileReader;
    }

    protected function assertColumnAndLinesNumbers($columnCount, $lineCount, CSVFileReader $csvFileReader, $withHeader = true)
    {
        $headers = $csvFileReader->getHeaders();
        $data = $csvFileReader->getData();

        if ($withHeader) {
            $this->assertCount($columnCount, $headers);
        } else {
            $this->assertNull($headers);
        }

        $this->assertCount($lineCount, $data);

        if ($data != null) {
            foreach ($data as $line) {
                $this->assertCount($columnCount, $line);
            }
        }
    }
}
