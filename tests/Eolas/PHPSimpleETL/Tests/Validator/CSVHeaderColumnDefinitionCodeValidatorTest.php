<?php

namespace Eolas\PHPSimpleETL\Tests\Validator;

use Eolas\PHPSimpleETL\DataDescriptor\ArrayDescriptor;
use Eolas\PHPSimpleETL\DataDescriptor\ColumnDefinition;
use Eolas\PHPSimpleETL\DataDescriptor\Type\DefaultTypeManagerFactory;
use Eolas\PHPSimpleETL\DataDescriptor\Type\TypeManager;
use Eolas\PHPSimpleETL\Validator\CSVHeaderColumnDefinitionCodeValidator;

class CSVHeaderColumnDefinitionCodeValidatorTest extends \PHPUnit_Framework_TestCase
{
    /** @var TypeManager */
    protected $typeManager;

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testEmpty()
    {
        $validator = new CSVHeaderColumnDefinitionCodeValidator();

        $validator->validate(array(), $this->getArrayDescriptor());
    }

    public function testAllColumnsValid()
    {
        $validator = new CSVHeaderColumnDefinitionCodeValidator();

        $data = array(0 => 'col1', 1 => 'col2', 2 => 'col3');

        $invalidColumns = $validator->validate($data, $this->getArrayDescriptor());

        $this->assertCount(0, $invalidColumns);
    }

    public function testAllColumnsInvalid()
    {
        $validator = new CSVHeaderColumnDefinitionCodeValidator();

        $header = array(0 => 'col4', 1 => 'col5', 2 => 'col6');

        $invalidColumns = $validator->validate($header, $this->getArrayDescriptor());

        $this->assertCount(3, $invalidColumns);

        $this->assertArrayHasKey(0, $invalidColumns);
        $this->assertEquals('col4', $invalidColumns[0]);

        $this->assertArrayHasKey(1, $invalidColumns);
        $this->assertEquals('col5', $invalidColumns[1]);

        $this->assertArrayHasKey(2, $invalidColumns);
        $this->assertEquals('col6', $invalidColumns[2]);
    }

    public function testPartiallyInvalid()
    {
        $validator = new CSVHeaderColumnDefinitionCodeValidator();

        $header = array(0 => 'col1', 1 => 'col5', 2 => 'col3');

        $invalidColumns = $validator->validate($header, $this->getArrayDescriptor());

        $this->assertCount(1, $invalidColumns);

        $this->assertArrayHasKey(1, $invalidColumns);
        $this->assertEquals('col5', $invalidColumns[1]);
    }

    public function testValidSubset()
    {
        $validator = new CSVHeaderColumnDefinitionCodeValidator();

        $header = array(0 => 'col1', 1 => 'col3');

        $invalidColumns = $validator->validate($header, $this->getArrayDescriptor());

        $this->assertCount(0, $invalidColumns);
    }

    protected function getArrayDescriptor()
    {
        $integerType = $this->typeManager->getType('integer');

        $arrayDescriptor = new ArrayDescriptor();

        $colDef = new ColumnDefinition('col1', $integerType);
        $arrayDescriptor->addColumnDefinition('col1', $colDef);

        $colDef = new ColumnDefinition('col2', $integerType);
        $arrayDescriptor->addColumnDefinition('col2', $colDef);

        $colDef = new ColumnDefinition('col3', $integerType);
        $arrayDescriptor->addColumnDefinition('col3', $colDef);

        return $arrayDescriptor;
    }

    protected function setUp()
    {
        $typeManagerFactory = new DefaultTypeManagerFactory();

        $this->typeManager = $typeManagerFactory->createTypeManager();
    }
}
