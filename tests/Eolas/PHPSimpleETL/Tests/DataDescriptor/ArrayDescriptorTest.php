<?php

namespace Eolas\PHPSimpleETL\Tests\DataDescriptor;

use Eolas\PHPSimpleETL\DataDescriptor\ArrayDescriptor;
use Eolas\PHPSimpleETL\DataDescriptor\ColumnDefinition;
use Eolas\PHPSimpleETL\DataDescriptor\Type\DefaultTypeManagerFactory;
use Eolas\PHPSimpleETL\DataDescriptor\Type\TypeManager;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Type;

class ArrayDescriptorTest extends \PHPUnit_Framework_TestCase
{
    /** @var TypeManager */
    protected $typeManager;

    public function testEmptyDescriptor()
    {
        $arrayDescriptor = new ArrayDescriptor();

        $columnIndexType = $arrayDescriptor->getColumnIndexType();

        $this->assertNull($columnIndexType);

        $descriptor = $arrayDescriptor->getDescriptor();

        $this->assertCount(0, $descriptor);
    }

    public function testAddNamedColumDefinition()
    {
        $arrayDescriptor = new ArrayDescriptor();

        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        $arrayDescriptor->addColumnDefinition('code', $simpleColDef);

        $this->checkNamedDescriptor($arrayDescriptor, $simpleColDef);
    }

    public function testAddPositionalColumDefinition()
    {
        $arrayDescriptor = new ArrayDescriptor();

        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        $arrayDescriptor->addColumnDefinition(0, $simpleColDef);

        $this->checkPositionalDescriptor($arrayDescriptor, $simpleColDef);
    }

    public function testInitWithNamedDescriptor()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        $arrayDescriptor = new ArrayDescriptor(['code' => $simpleColDef]);

        $this->checkNamedDescriptor($arrayDescriptor, $simpleColDef);
    }

    public function testInitWithPositionalDescriptor()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        $arrayDescriptor = new ArrayDescriptor([0 => $simpleColDef]);

        $this->checkPositionalDescriptor($arrayDescriptor, $simpleColDef);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testPositionalCoherence()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        // init as positional
        $arrayDescriptor = new ArrayDescriptor([0 => $simpleColDef]);

        // add an named column
        $simpleColDef = new ColumnDefinition('code2', $this->getIntegerType());
        $arrayDescriptor->addColumnDefinition('code2', $simpleColDef);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testNamedCoherence()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')], 'label');

        // init as positional
        $arrayDescriptor = new ArrayDescriptor(['code' => $simpleColDef]);

        // add an named column
        $simpleColDef = new ColumnDefinition('code2', $this->getIntegerType());
        $arrayDescriptor->addColumnDefinition(1, $simpleColDef);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testColumnDefinitionCodeUnicityOnInit()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType());
        $simpleColDef2 = new ColumnDefinition('code', $this->getFloatType());
        $arrayDescriptor = new ArrayDescriptor([$simpleColDef, $simpleColDef2]);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testColumnDefinitionCodeUnicityOnAdd()
    {
        $simpleColDef = new ColumnDefinition('code', $this->getIntegerType());
        $arrayDescriptor = new ArrayDescriptor([$simpleColDef]);

        $simpleColDef2 = new ColumnDefinition('code', $this->getFloatType());
        $arrayDescriptor->addColumnDefinition(1, $simpleColDef2);
    }

    protected function setUp()
    {
        $typeManagerFactory = new DefaultTypeManagerFactory();

        $this->typeManager = $typeManagerFactory->createTypeManager();
    }

    /**
     * @param string $code
     * @return \Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType|null
     */
    protected function getType($code)
    {
        return $this->typeManager->getType($code);
    }

    /**
     * @return \Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType|null
     */
    protected function getFloatType()
    {
        return $this->typeManager->getType('float');
    }

    /**
     * @return \Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType|null
     */

    protected function getIntegerType()
    {
        return $this->getType('integer');
    }

    /**
     * @param $arrayDescriptor
     * @param $simpleColDef
     */
    protected function checkNamedDescriptor($arrayDescriptor, $simpleColDef)
    {
        $columnIndexType = $arrayDescriptor->getColumnIndexType();

        $this->assertEquals(ArrayDescriptor::NAMED_INDEX, $columnIndexType);

        $descriptor = $arrayDescriptor->getDescriptor();

        $this->assertCount(1, $descriptor);

        $this->assertTrue(isset($descriptor['code']));

        $descriptorColumn = $descriptor['code'];

        $this->assertEquals($simpleColDef, $descriptorColumn);
    }

    /**
     * @param $arrayDescriptor
     * @param $simpleColDef
     */
    protected function checkPositionalDescriptor($arrayDescriptor, $simpleColDef)
    {
        $columnIndexType = $arrayDescriptor->getColumnIndexType();

        $this->assertEquals(ArrayDescriptor::POSITIONAL_INDEX, $columnIndexType);

        $descriptor = $arrayDescriptor->getDescriptor();

        $this->assertCount(1, $descriptor);

        $this->assertTrue(isset($descriptor[0]));

        $descriptorColumn = $descriptor[0];

        $this->assertEquals($simpleColDef, $descriptorColumn);
    }
}
