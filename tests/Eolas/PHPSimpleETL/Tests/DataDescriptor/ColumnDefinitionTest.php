<?php

namespace Eolas\PHPSimpleETL\Tests\DataDescriptor;

use Eolas\PHPSimpleETL\DataDescriptor\ColumnDefinition;
use Eolas\PHPSimpleETL\DataDescriptor\Type\DefaultTypeManagerFactory;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Type;

class ColumnDefinitionTest extends \PHPUnit_Framework_TestCase
{
    /** @var TypeManager */
    protected $typeManager;

    public function testValidColumnDefinition()
    {
        $intCol = new ColumnDefinition('code', $this->getType('integer'), true, array(), 'label');
        $strCol = new ColumnDefinition('code', $this->getType('float'), false);
        $strCol = new ColumnDefinition('code', $this->getType('string'), false, array());
        //TODO:
/*
        $dateCol = new ColumnDefinition('code', $this->getType('date'));
        $datetimeCol = new ColumnDefinition('code', $this->getType('datetime'));
        $arrayCol = new ColumnDefinition('code', $this->getType('array'));
*/
    }

    public function testUnexpectedColumnDefinitionType()
    {
        $type = $this->getType('toto');

        $this->assertNull($type);

        // can't create a ColumnDefinition without not null type
        //$arrayCol = new ColumnDefinition('code', $type);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnexpectedCodeArgumentDefinitionType()
    {
        $arrayCol = new ColumnDefinition(111, $this->getIntegerType());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnexpectedNullableArgumentDefinitionType()
    {
        $arrayCol = new ColumnDefinition('code', $this->getIntegerType(), 0);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnexpectedConstraintsArgumentDefinitionType()
    {
        $arrayCol = new ColumnDefinition('code', $this->getIntegerType(), true, ['a', 'b']);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnexpectedLabelArgumentDefinitionType()
    {
        $arrayCol = new ColumnDefinition('code', $this->getIntegerType(), true, [], 111);
    }

    public function testWithConstraints()
    {
        $arrayCol = new ColumnDefinition('code', $this->getIntegerType(), true, [new Blank(), new Type('integer')]);
    }

    protected function setUp()
    {
        $typeManagerFactory = new DefaultTypeManagerFactory();

        $this->typeManager = $typeManagerFactory->createTypeManager();
    }

    /**
     * @param string $code
     * @return \Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType|null
     */
    protected function getType($code)
    {
        return $this->typeManager->getType($code);
    }

    /**
     * @return \Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType|null
     */

    protected function getIntegerType()
    {
        return $this->getType('integer');
    }
}
