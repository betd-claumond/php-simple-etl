<?php

namespace Eolas\PHPSimpleETL\Tests\DataDescriptor\Type;

use Eolas\PHPSimpleETL\DataDescriptor\Type\SimpleType;

class SimpleTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testBoolType()
    {
        $simpleType = new SimpleType('bool');

        $this->assertTrue($simpleType->isValidStringValue('0'));
        $this->assertTrue($simpleType->isValidStringValue('1'));
        $this->assertTrue($simpleType->isValidStringValue('true'));
        $this->assertTrue($simpleType->isValidStringValue('false'));

        $this->assertFalse($simpleType->isValidStringValue('1.11'));
        $this->assertFalse($simpleType->isValidValue(1.11));
        $this->assertFalse($simpleType->isValidValue('1.11'));

        $this->assertFalse($simpleType->isValidStringValue('10'));
        $this->assertFalse($simpleType->isValidValue((float) 10));
        $this->assertFalse($simpleType->isValidValue('10'));

        $this->assertFalse($simpleType->isValidStringValue('aaa'));
        $this->assertFalse($simpleType->isValidValue('aaa'));
    }

    public function testIntegerTypes()
    {
        $simpleType = new SimpleType('integer');
        $this->assertIntegerTypes($simpleType);

        $simpleType = new SimpleType('int');
        $this->assertIntegerTypes($simpleType);

        $simpleType = new SimpleType('long');
        $this->assertIntegerTypes($simpleType);
    }

    public function testFloatTypes()
    {
        $simpleType = new SimpleType('float');
        $this->assertFloatTypes($simpleType);

        $simpleType = new SimpleType('real');
        $this->assertFloatTypes($simpleType);

        $simpleType = new SimpleType('double');
        $this->assertFloatTypes($simpleType);
    }

    public function testStringType()
    {
        $simpleType = new SimpleType('string');

        $this->assertTrue($simpleType->isValidStringValue('aaa'));
        $this->assertTrue($simpleType->isValidValue('aaa'));

        $this->assertTrue($simpleType->isValidStringValue('0'));
        $this->assertTrue($simpleType->isValidStringValue('1'));
        $this->assertTrue($simpleType->isValidStringValue('true'));
        $this->assertTrue($simpleType->isValidStringValue('false'));

        $this->assertTrue($simpleType->isValidStringValue('1.11'));
        $this->assertFalse($simpleType->isValidValue(1.11));
        $this->assertTrue($simpleType->isValidValue('1.11'));

        $this->assertTrue($simpleType->isValidStringValue('10'));
        $this->assertFalse($simpleType->isValidValue((float) 10));
        $this->assertTrue($simpleType->isValidValue('10'));
    }

    protected function assertIntegerTypes(SimpleType $simpleType)
    {
        $this->assertTrue($simpleType->isValidStringValue('10'));
        $this->assertEquals(10, $simpleType->convertFromString('10'));
        $this->assertTrue($simpleType->isValidValue(10));
        $this->assertFalse($simpleType->isValidValue('10'));

        $this->assertFalse($simpleType->isValidStringValue('aaa'));
        $this->assertFalse($simpleType->isValidValue('aaa'));

        $this->assertFalse($simpleType->isValidStringValue('1.11'));
        $this->assertFalse($simpleType->isValidValue(1.11));
    }

    protected function assertFloatTypes(SimpleType $simpleType)
    {
        $this->assertTrue($simpleType->isValidStringValue('1.11'));
        $this->assertEquals(1.11, $simpleType->convertFromString('1.11'));
        $this->assertTrue($simpleType->isValidValue(1.11));
        $this->assertFalse($simpleType->isValidValue('1.11'));

        $this->assertTrue($simpleType->isValidStringValue('10'));
        $this->assertEquals((float) 10, $simpleType->convertFromString('10'));
        $this->assertTrue($simpleType->isValidValue((float) 10));
        $this->assertFalse($simpleType->isValidValue('10'));

        $this->assertFalse($simpleType->isValidStringValue('aaa'));
        $this->assertFalse($simpleType->isValidValue('aaa'));
    }
}
