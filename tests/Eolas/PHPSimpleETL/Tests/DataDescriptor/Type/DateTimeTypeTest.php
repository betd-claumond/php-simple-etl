<?php

namespace Eolas\PHPSimpleETL\Tests\DataDescriptor\Type;


use Eolas\PHPSimpleETL\DataDescriptor\Type\DateTimeType;

class DateTimeTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testDateType()
    {
        $dateType = new DateTimeType('date', 'Y-m-d');

        $this->assertTrue($dateType->isValidStringValue('2015-06-13'));
        $date = $dateType->convertFromString('2015-06-13');
        $this->assertEquals(new \DateTime('2015-06-13 00:00:00'), $date);
        $this->assertTrue($dateType->isValidValue($date));
        $this->assertTrue($dateType->isValidValue(new \DateTime()));

        $this->assertFalse($dateType->isValidStringValue('2015/06/13'));
        $this->assertFalse($dateType->isValidStringValue('aaaaa'));
    }

    public function testDateTypeFrenchFormat()
    {
        $dateType = new DateTimeType('date', 'd/m/Y');

        $this->assertTrue($dateType->isValidStringValue('13/06/2015'));
        $date = $dateType->convertFromString('13/06/2015');
        $this->assertEquals(new \DateTime('2015-06-13 00:00:00'), $date);
        $this->assertTrue($dateType->isValidValue($date));
        $this->assertTrue($dateType->isValidValue(new \DateTime()));

        $this->assertFalse($dateType->isValidStringValue('2015/06/13'));
        $this->assertFalse($dateType->isValidStringValue('aaaaa'));
    }

    public function testDateTimeType()
    {
        $dateType = new DateTimeType('datetime', 'Y-m-d H:i:s');

        $this->assertTrue($dateType->isValidStringValue('2015-06-13 13:59:00'));
        $date = $dateType->convertFromString('2015-06-13 13:59:00');
        $this->assertEquals(new \DateTime('2015-06-13 13:59:00'), $date);
        $this->assertTrue($dateType->isValidValue($date));
        $this->assertTrue($dateType->isValidValue(new \DateTime()));

        $this->assertFalse($dateType->isValidStringValue('2015/06/13 13:59:00'));
        $this->assertFalse($dateType->isValidStringValue('aaaaa'));
    }

    public function testDateTimeTypeFrenchFormat()
    {
        $dateType = new DateTimeType('datetime', 'd/m/Y H:i:s');

        $this->assertTrue($dateType->isValidStringValue('13/06/2015 13:59:00'));
        $date = $dateType->convertFromString('13/06/2015 13:59:00');
        $this->assertEquals(new \DateTime('2015-06-13 13:59:00'), $date);
        $this->assertTrue($dateType->isValidValue($date));
        $this->assertTrue($dateType->isValidValue(new \DateTime()));

        $this->assertFalse($dateType->isValidStringValue('2015/06/13 13:59:00'));
        $this->assertFalse($dateType->isValidStringValue('aaaaa'));
    }
}
