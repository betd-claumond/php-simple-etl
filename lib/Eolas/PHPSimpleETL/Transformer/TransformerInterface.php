<?php

namespace Eolas\PHPSimpleETL\Transformer;

interface TransformerInterface
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data);
}
