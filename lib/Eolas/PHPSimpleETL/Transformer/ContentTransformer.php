<?php

namespace Eolas\PHPSimpleETL\Transformer;

class ContentTransformer implements TransformerInterface
{
    /**
     * @var TransformerInterface
     */
    private $transformer;

    public function __construct(TransformerInterface $tranformer)
    {
        $this->transformer = $tranformer;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data)
    {
        $transformedData = array();

        $transformer = $this->getTransformer();
        foreach ($data as $key => $value) {
            $transformedData[$key] = $transformer->transform($value);
        }

        return $transformedData;
    }

    /**
     * @return TransformerInterface
     */
    public function getTransformer()
    {
        return $this->transformer;
    }
}
