<?php

namespace Eolas\PHPSimpleETL\Transformer;

class ArrayColumnRemoveTransformer
{
    /**
     * @var array
     */
    private $columnIndexes;

    /**
     * @param array $columnIndexes
     */
    public function __construct(array $columnIndexes)
    {
        $this->columnIndexes = $columnIndexes;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data)
    {
        foreach ($this->getColumnIndexes() as $columnIndex) {
            unset($data[$columnIndex]);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getColumnIndexes()
    {
        return $this->columnIndexes;
    }
}
