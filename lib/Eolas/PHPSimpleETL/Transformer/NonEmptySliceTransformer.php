<?php

namespace Eolas\PHPSimpleETL\Transformer;

class NonEmptySliceTransformer implements TransformerInterface
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data)
    {
        $indexLastNonEmptyValue = $this->computeIndexLastNonEmptyValue($data);

        // if only empty values, we return an empty array
        if ($indexLastNonEmptyValue === null) {
            return array();
        }

        // the size of the final array
        $finalLength = $indexLastNonEmptyValue + 1;

        // if there are empty values at the end of line, slice
        if ($finalLength !== count($data)) {
            $sliceTransformer = new SliceTransformer($finalLength);
            $data = $sliceTransformer->transform($data);
        }

        return $data;
    }

    protected function computeIndexLastNonEmptyValue(array $data)
    {
        $indexLastNonEmptyValue = null;

        // search for the last column with an non empty header
        $columnIndex = 0;
        foreach ($data as $columnValue) {
            $columnValue = trim($columnValue);

            if (!empty($columnValue)) {
                $indexLastNonEmptyValue = $columnIndex;
            }

            ++$columnIndex;
        }

        return $indexLastNonEmptyValue;
    }
}
