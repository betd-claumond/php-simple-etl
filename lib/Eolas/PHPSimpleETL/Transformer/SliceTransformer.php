<?php

namespace Eolas\PHPSimpleETL\Transformer;

class SliceTransformer implements TransformerInterface
{
    /** @var int|null|false */
    private $columnCount;

    /**
     * @param int|null|false $columnCount
     */
    public function __construct($columnCount)
    {
        $this->columnCount = $columnCount;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function transform(array $data)
    {
        $columnCount = $this->getColumnCount();

        if ($columnCount === null || $columnCount === false) {
            return $data;
        }

        $transformedData = array_slice($data, 0, $columnCount, true);

        return $transformedData;
    }

    /**
     * @return int|null|false
     */
    public function getColumnCount()
    {
        return $this->columnCount;
    }
}
