<?php

namespace Eolas\PHPSimpleETL\Validator;

use Eolas\PHPSimpleETL\DataDescriptor\ArrayDescriptor;

class CSVHeaderColumnDefinitionCodeValidator
{
    public function validate(array $header, ArrayDescriptor $arrayDescriptor)
    {
        if (count($header) == 0) {
            throw new \InvalidArgumentException('header must not be empty');
        }

        $descriptor = $arrayDescriptor->getDescriptor();

        $codes = array();
        foreach ($descriptor as $columnDefinition) {
            $codes[] = $columnDefinition->getCode();
        }

        $invalidColumns = array();
        foreach ($header as $columnIndex => $headerName) {
            if (!in_array($headerName, $codes)) {
                $invalidColumns[$columnIndex] = $headerName;
            }
        }

        return $invalidColumns;
    }
}
