<?php

namespace Eolas\PHPSimpleETL\DataDescriptor\Type;

use Symfony\Component\Validator\Constraints\DateTime;

class DateTimeType extends AbstractType
{
    /** @var string */
    private $format;

    public function __construct($code, $format)
    {
        parent::__construct($code);

        $this->format = $format;
    }

    public function isValidStringValue($stringValue)
    {
        $convertedValue = $this->convertFromString($stringValue);

        return $convertedValue != false;
    }

    public function convertFromString($stringValue)
    {
        $dateTime = \DateTime::createFromFormat($this->getFormat(), $stringValue);

        // we reset the hour minute second for date type
        if ($dateTime && $this->getCode() == 'date') {
            $dateTime->setTime(0, 0, 0);
        }

        return $dateTime;
    }

    public function isValidValue($value)
    {
        return $value instanceof \DateTime;
    }

    protected function checkCode($code)
    {
        $allowedCodes = array('date', 'datetime');

        if (!in_array($code, $allowedCodes)) {
            throw new \InvalidArgumentException("The code '$code' is not valid for a DateTimeType");
        }
    }

    // GETTERS

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }
}
