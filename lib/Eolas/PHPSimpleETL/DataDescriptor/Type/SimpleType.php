<?php

namespace Eolas\PHPSimpleETL\DataDescriptor\Type;

use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;

class SimpleType extends AbstractType
{
    public function isValidStringValue($stringValue)
    {
        $isValid = false;
        $convertedValue = $this->convertFromString($stringValue);

        switch ($this->getCode()) {
            case 'bool':
                $isValid = filter_var($stringValue, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null;
                break;
            case 'int':
            case 'integer':
            case 'long':
            case 'string':
                $isValid = $stringValue == "$convertedValue";
                break;
            case 'float':
            case 'real':
            case 'double':
                $isValid = is_numeric($stringValue);
                break;
        }

        return $isValid;
    }

    public function convertFromString($stringValue)
    {
        $convertedValue = $stringValue;

        switch ($this->getCode()) {
            case 'bool':
                $convertedValue = function_exists('boolval') ? boolval($stringValue) : (bool) $stringValue;
                break;
            case 'float':
            case 'real':
                $convertedValue = floatval($stringValue);
                break;
            case 'double':
                $convertedValue = doubleval($stringValue);
                break;
            case 'int':
            case 'integer':
            case 'long':
                $convertedValue = intval($stringValue);
                break;
            case 'string':
                $convertedValue = $stringValue;
                break;
        }

        return $convertedValue;
    }

    public function isValidValue($value)
    {
        if (!is_scalar($value)) {
            return false;
        }

        $validator = Validation::createValidator();

        $typeConstraint = new Type(array('type' => $this->getCode()));

        $violations = $validator->validate($value, $typeConstraint);

        return count($violations) == 0;
    }

    protected function checkCode($code)
    {
        $allowedCodes = array('bool', 'float', 'real', 'double', 'int', 'integer', 'long', 'string');

        if (!in_array($code, $allowedCodes)) {
            throw new \InvalidArgumentException("The code '$code' is not valid for a SimpleType");
        }
    }
}
