<?php

namespace Eolas\PHPSimpleETL\DataDescriptor\Type;

class TypeManager
{
    /** @var array $types */
    private $types;

    /**
     * @param AbstractType $type
     *
     * @return $this
     */
    public function addType(AbstractType $type)
    {
        $code = $type->getCode();

        if (isset($types[$code])) {
            throw new \InvalidArgumentException("The type of code '$code' already exists");
        }

        $this->types[$code] = $type;

        return $this;
    }

    /**
     * @param string $code
     *
     * @return AbstractType|null
     */
    public function getType($code)
    {
        return isset($this->types[$code]) ? $this->types[$code] : null;
    }
}
