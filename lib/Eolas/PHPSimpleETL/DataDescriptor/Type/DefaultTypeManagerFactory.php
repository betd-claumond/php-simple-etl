<?php

namespace Eolas\PHPSimpleETL\DataDescriptor\Type;

class DefaultTypeManagerFactory
{
    /**
     * @return TypeManager
     */
    public function createTypeManager()
    {
        $typeManager = new TypeManager();

        $typeManager
            ->addType(new SimpleType('bool'))
            ->addType(new SimpleType('integer'))
            ->addType(new SimpleType('float'))
            ->addType(new SimpleType('string'))
            ->addType(new DateTimeType('date', 'Y-m-d'))
            ->addType(new DateTimeType('datetime', 'Y-m-d H:i:s'))
        ;

        //TODO: 'array'

        return $typeManager;
    }
}
