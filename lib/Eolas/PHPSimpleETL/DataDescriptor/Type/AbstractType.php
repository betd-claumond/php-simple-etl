<?php

namespace Eolas\PHPSimpleETL\DataDescriptor\Type;

abstract class AbstractType
{
    /** @var string $code */
    private $code;

    /**
     * @param string $code
     */
    public function __construct($code)
    {
        $this->checkCode($code);

        $this->code = $code;
    }

    abstract public function isValidStringValue($stringValue);

    abstract public function convertFromString($stringValue);

    abstract public function isValidValue($value);

    protected function checkCode($code)
    {
        // do nothing
    }

    public function getCode()
    {
        return $this->code;
    }
}
