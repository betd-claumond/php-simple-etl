<?php

namespace Eolas\PHPSimpleETL\DataDescriptor;

use Eolas\PHPSimpleETL\DataDescriptor\Type\AbstractType;
use Symfony\Component\Validator\Constraint;

class ColumnDefinition
{
    /** @var string  */
    private $code;

    /** @var AbstractType */
    private $type;

    /** @var bool */
    private $nullable;

    /** @var Constraint[] $constraints */
    private $constraints;

    /** @var string */
    private $label;

    /**
     * @param string       $code
     * @param AbstractType $type
     * @param bool         $nullable
     * @param Constraint[] $constraints
     * @param string       $label
     */
    public function __construct($code, AbstractType $type, $nullable = true, array $constraints = array(), $label = null)
    {
        $this->code = $code;
        $this->type = $type;
        $this->nullable = $nullable;
        $this->constraints = $constraints;
        $this->label = $label;

        $this->checkColumnDefinition();
    }

    protected function checkColumnDefinition()
    {
        if (!is_string($this->code)) {
            throw new \InvalidArgumentException('code must be a string');
        }

        if (!is_bool($this->nullable)) {
            throw new \InvalidArgumentException('nullable must be a bool');
        }

        $this->checkConstraints();

        if ($this->label != null && !is_string($this->label)) {
            throw new \InvalidArgumentException('label must be a string');
        }
    }

    protected function checkConstraints()
    {
        foreach ($this->constraints as $constraint) {
            if (!$constraint instanceof Constraint) {
                throw new \InvalidArgumentException('constraints must be an array of Constraint');
            }
        }
    }

    // GETTERS / SETTERS

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isNullable()
    {
        return $this->nullable;
    }

    /**
     * @param bool $nullable
     */
    public function setNullable($nullable)
    {
        $this->nullable = $nullable;
    }

    /**
     * @return \Symfony\Component\Validator\Constraint[]
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @param \Symfony\Component\Validator\Constraint[] $constraints
     */
    public function setConstraints($constraints)
    {
        $this->constraints = $constraints;
    }
}
