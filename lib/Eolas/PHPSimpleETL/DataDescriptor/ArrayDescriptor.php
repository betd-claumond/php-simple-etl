<?php

namespace Eolas\PHPSimpleETL\DataDescriptor;

class ArrayDescriptor
{
    const POSITIONAL_INDEX = 1;
    const NAMED_INDEX = 2;

    private $descriptor;

    /** @var int|null  */
    private $columnIndexType = null;

    public function __construct(array $descriptor = array())
    {
        $this->descriptor = $descriptor;

        $this->initColumnIndexType();

        $this->checkDescriptor();
    }

    public function addColumnDefinition($columnIndex, ColumnDefinition $columnDefinition)
    {
        $this->checkColumnIndex($columnIndex);
        $this->checkColumnCode($columnDefinition->getCode());

        $this->descriptor[$columnIndex] = $columnDefinition;

        // if first column definition
        if (count($this->descriptor) === 1) {
            $this->initColumnIndexType();
        }
    }

    protected function checkDescriptor()
    {
        $codes = array();
        foreach ($this->getDescriptor() as $columnIndex => $columnDefinition) {
            $this->checkColumnIndex($columnIndex);

            if (!$columnDefinition instanceof ColumnDefinition) {
                throw new \InvalidArgumentException('an array of ColumnDefinition is expected');
            }

            // check unicity for ColumnDefinition.code
            $code = $columnDefinition->getCode();
            if (in_array($code, $codes)) {
                throw new \InvalidArgumentException("the ColumnDefinition.code '$code' is already used");
            }
            $codes[] = $code;
        }
    }

    protected function initColumnIndexType()
    {
        if (count($this->descriptor) > 0) {
            // look at the first index in the data descriptor
            $columnIndex = array_keys($this->descriptor)[0];

            // determine if it is named or positional indexed data
            if (is_integer($columnIndex)) {
                $this->columnIndexType = static::POSITIONAL_INDEX;
            } elseif (is_string($columnIndex)) {
                $this->columnIndexType = static::NAMED_INDEX;
            }
        }
    }

    protected function checkColumnIndex($columnIndex)
    {
        switch ($this->columnIndexType) {
            case static::POSITIONAL_INDEX:
                if (!is_int($columnIndex)) {
                    throw new \InvalidArgumentException('columnIndex must be an int in positional indexed data');
                }
                break;
            case static::NAMED_INDEX:
                if (!is_string($columnIndex)) {
                    throw new \InvalidArgumentException('columnIndex must be a string in named indexed data');
                }
                break;
            default:
                if (!(is_integer($columnIndex) || is_string($columnIndex))) {
                    throw new \InvalidArgumentException('columnIndex must be either an int or a string');
                }
                break;
        }
    }

    protected function checkColumnCode($columnCode)
    {
        $columns = array_filter($this->descriptor, function (ColumnDefinition $columnDefinition) use ($columnCode) {
            return $columnDefinition->getCode() == $columnCode;
        });

        // if a column with this code has been found
        if (count($columns) > 0) {
            throw new \InvalidArgumentException("the ColumnDefinition.code '$columnCode' is already used");
        }
    }

    // GETTERS / SETTERS

    /**
     * @return ColumnDefinition[]
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * @return int|null
     */
    public function getColumnIndexType()
    {
        return $this->columnIndexType;
    }
}
