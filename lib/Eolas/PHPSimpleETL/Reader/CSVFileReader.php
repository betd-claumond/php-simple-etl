<?php

namespace Eolas\PHPSimpleETL\Reader;

use Eolas\PHPSimpleETL\Transformer\TransformerInterface;

class CSVFileReader
{
    // CONFIGURATION

    /**
     * @var string
     */
    private $separator = ';';

    /**
     * @var int|null|false
     */
    private $lineForHeader = 0;

    /**
     * @var string
     */
    private $filepath = null;

    /**
     * @var TransformerInterface[]
     */
    private $headerTransformers = array();

    /**
     * @var TransformerInterface[]
     */
    private $dataTransformers = array();

    // STATE

    /**
     * @var resource
     */
    private $handle = null;

    /**
     * @var int
     */
    private $currentLine = 0;

    /**
     * @var array|null
     */
    private $header = null;

    /**
     * @var array|null
     */
    private $data;

    // PUBLIC METHODS

    /**
     * @param string $filepath
     */
    public function __construct($filepath)
    {
        $this->filepath = $filepath;
    }

    public function init()
    {
        $this->handle = fopen($this->filepath, 'r');

        return $this->handle !== false;
    }

    public function reset()
    {
        $this->end();

        $this->init();
    }

    public function readHeader()
    {
        // if there is a header
        if ($this->lineForHeader !== null && $this->lineForHeader !== false) {
            // we read forward to the header line
            while (($line = $this->readLine()) !== false
                && $this->currentLine !== $this->lineForHeader) {
                ++$this->currentLine;
            }

            // if an header has been found
            if ($line !== false) {
                $this->header = $line;
            }
        }

        $this->applyHeaderTransformers();
    }

    public function read()
    {
        while (($line = $this->readLine()) !== false) {
            $this->data[] = $line;

            ++$this->currentLine;
        }

        $this->applyDataTransformers();
    }

    public function end()
    {
        // for reset, we close and reopen
        if ($this->handle !== null && $this->handle !== false) {
            fclose($this->handle);
        }

        $this->resetState();
    }

    public function addHeaderTransformer(TransformerInterface $transformer)
    {
        $this->headerTransformers[] = $transformer;
    }

    public function addDataTransformer(TransformerInterface $transformer)
    {
        $this->dataTransformers[] = $transformer;
    }

    public function getColumnCount()
    {
        return count($this->header) > 0 ? count($this->header) : null;
    }

    // PROTECTED METHODS

    protected function resetState()
    {
        $this->handle = null;
        $this->currentLine = 0;
        $this->header = null;
        $this->data = null;
    }

    protected function readLine()
    {
        return fgetcsv($this->handle, 0, $this->separator);
    }

    protected function applyHeaderTransformers()
    {
        if ($this->header != null) {
            foreach ($this->getHeaderTransformers() as $transformer) {
                $this->header = $transformer->transform($this->header);
            }
        }
    }

    protected function applyDataTransformers()
    {
        if ($this->data != null) {
            foreach ($this->getDataTransformers() as $transformer) {
                $this->data = $transformer->transform($this->data);
            }
        }
    }

    // GETTERS / SETTERS

    public function getHeaders()
    {
        return $this->header;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @param int|null|false $lineForHeader
     */
    public function setLineForHeader($lineForHeader)
    {
        $this->lineForHeader = $lineForHeader;
    }

    /**
     * @return \Eolas\PHPSimpleETL\Transformer\TransformerInterface[]
     */
    public function getHeaderTransformers()
    {
        return $this->headerTransformers;
    }

    /**
     * @return \Eolas\PHPSimpleETL\Transformer\TransformerInterface[]
     */
    public function getDataTransformers()
    {
        return $this->dataTransformers;
    }
}
